# -*- coding: utf8 -*-
from __future__ import unicode_literals, print_function

import datetime
import json
import logging
import os
import re
#import logging

from django.utils import timezone
from django.utils import translation
from django.utils.module_loading import import_string
from django.utils.six import iteritems

BASE_PATH = os.path.abspath(os.path.dirname(__file__))


class ImportData(object):
    title = None
    required = None
    object = None
    many = False

    def __init__(self, language):
        self.int_fields = []
        self.image_fields = (('image', 'images/'),)
        self.date_fields = []
        self.fk_fields = []
        self.mtm_fields = []
        self.file_name = 'textpages'
        self._file_path = 'pr/{}.pr'
        self.language = language

        # Default
        self.fields = {}
        self.TranslatableModel = None
        self.many = bool(self.many)

    @property
    def file_path(self):
        return os.path.join(BASE_PATH, self._file_path)

    def get_data_json(self):
        data_file = open(self.file_path.format(self.file_name))
        data_json = json.loads(data_file.read())
        return data_json

    def get_default_fields(self, item):
        return item

    def struct_data(self):
        data_json = self.get_data_json()
        result = []
        if self.title in data_json:
            for item in data_json[self.title]:
                try:
                    required_fields = {key: item[key] for key in self.required}
                    required_fields.update({"defaults": self.get_default_fields(item)})
                    self.fields = required_fields
                    result.append(self.format_data())
                except ValueError as detail:
                    logging.warning('Key value not found: {}'.format(detail))
        return result

    def format_data(self):
        # убираем mtm поля из основной выборки
        mtm_fields = {key[0]: self.fields['defaults'].pop(key[0]) for key in self.mtm_fields if
                      key[0] in self.fields['defaults']}
        # форматируем поля по типам
        self.format_int_fields()
        self.format_image_fields()
        self.format_date_fields()
        self.format_fk_fields()
        # создаем или обновляем объект
        new_object = self.update_or_create(self.object, **self.fields)
        # обновляем mtm поля
        self.format_mtm_fields(new_object, mtm_fields)
        # callback
        if callable(getattr(self, 'callback_func', None)):
            self.callback_func(new_object)
        return new_object

    def format_int_fields(self):
        for int_field in self.int_fields:
            try:
                self.fields['defaults'][int_field] = self.format_int(self.fields['defaults'][int_field])
            except KeyError:
                pass

    def format_image_fields(self):
        for image_field_name, image_field_path in self.image_fields:
            try:
                self.fields['defaults'][image_field_name] = self.format_image(image_field_path,
                                                                              self.fields['defaults'][image_field_name])
            except KeyError:
                pass

    def format_date_fields(self):
        for date_field_name, date_field_format in self.date_fields:
            try:
                self.fields['defaults'][date_field_name] = self.format_date(self.fields['defaults'][date_field_name],
                                                                            date_field_format)
            except KeyError:
                pass

    def format_fk_fields(self):
        for fk_field in self.fk_fields:
            try:
                self.fields['defaults'][fk_field[0]] = self.format_fk(fk_field[1], fk_field[2],
                                                                      self.fields['defaults'][fk_field[0]], fk_field)
            except KeyError:
                pass
    
    def format_mtm_fields(self, new_object, mtm_fields):
        for mtm_field in self.mtm_fields:
            try:
#                logger = logging.getLoggger(name=__name__)
#                logging.captureWarnings(True)
                add_mtm_objects = self.format_mtm(mtm_field[1], mtm_field[2], mtm_fields[mtm_field[0]], mtm_field)
                setattr(new_object[0], mtm_field[0], add_mtm_objects)
#                logger.error(str(new_object[0]), str(mtm_field[0]))
            except KeyError:
                pass
                

    def callback_func(self, new_object):
        pass

    def run(self):
        assert self.title is not None, 'TITLE Пусто'
        assert self.required is not None, 'REQUIRED Пусто'
        assert self.object is not None, 'OBJECT Пусто'

        _path = self.file_path.format(self.file_name)
        if not os.path.exists(os.path.abspath(os.path.join(os.path.dirname(__file__), _path))):
            raise AssertionError('Файл не найден')

        from django.db import models

#        self.TranslatableModel = import_string('parler.models.TranslatableModel') or models.Model

        translation.activate(self.language)

        return self.struct_data()

    # # #

    @staticmethod
    def get_translated_data(instance, **data):
        payload = data.copy()
        translations_fields = instance.translations.rel.to
        defaults = payload.pop('defaults', {})
        for field in filter(lambda _field: hasattr(translations_fields, _field), payload.keys()):
            payload['translations__{}'.format(field)] = payload.pop(field)
            defaults['translations__{}'.format(field)] = defaults.pop(field)
        payload.setdefault('defaults', defaults)
        return payload

    def update_or_create(self, instance, **data):
        if not hasattr(instance, 'translations'):
            return instance.objects.update_or_create(**data)

        payload = self.get_translated_data(instance, **data.copy())
        defaults = payload.pop('defaults')
        qset = instance.objects.distinct().filter(**payload)
        if qset.exists():
            obj = None
            _length = 1 if not self.many else qset.count()
            for obj in qset.all()[:_length]:
                obj.set_current_language(self.language)
                for field, value in iteritems(defaults):
                    setattr(obj, field, value)
                obj.save()
            return obj, False

        data = dict(map(lambda (k, v): (k.replace('translations__', ''), v), iteritems(defaults)))
        return instance.objects.language(self.language).create(**data), True

    def get_or_create(self, instance, **data):
        if not hasattr(instance, 'translations'):
            return instance.objects.get_or_create(**data)

        payload = self.get_translated_data(instance, **data)
        defaults = payload.pop('defaults')
        qset = instance.objects.distinct().filter(**payload)
        if qset.exists():
            return qset.first()

        data = dict(map(lambda (k, v): (k.replace('translations__', ''), v), iteritems(defaults)))
        return instance.objects.language(self.language).create(**data)

    def format_mtm(self, object, key, values, mtm_field):
        if isinstance(values, unicode) or isinstance(values, str):
            values = [i.strip() for i in values.split(',')]

        try:
            temp_import = mtm_field[3](self.language)
            result = []
            for mtm_item in values:
                if mtm_item:
                    temp_import.fields = {key: (lambda: mtm_item[key] if isinstance(mtm_item, dict) else mtm_item)(),
                                          'defaults': (lambda: mtm_item if isinstance(mtm_item, dict) else {})()}
                    result.append(temp_import.format_data()[0])

            return result

        except IndexError:
            """ 
                расшифровка жести ниже: генерируем список объектов, 
                полученных при помощи object.objects.get_or_create(**{key:lambda1,defaults:lambda2})
                lambda1 - отдает строку, получаемую либо непосредственно, либо, если mtm_item - словарь, то по ключу key
                lambda2 - отдает либо словарь mtm_item, либо {}
                values - все значения, по которым надо получить объекты
            """
            return [self.get_or_create(object, **{
                key: (lambda: mtm_item[key] if isinstance(mtm_item, dict) else mtm_item)(),
                'defaults': (lambda: mtm_item if isinstance(mtm_item, dict) else {})()
            })[0] for mtm_item in values if mtm_item]

    def format_int(self, num):
        return int(re.sub(r'\D+', '', num))

    def format_image(self, folder, value):
        return folder + value

    def format_fk(self, object, field, value, fk_field):
        if value:
            defaults = {}
            # если получаем dict, используем все переданные поля
            if isinstance(value, dict):
                defaults = value;
                value = value[field]

            # если передается объект типа ImportData, при добавлении используем его
            try:
                temp_import = fk_field[3]()
                temp_import.fields = {key: item[key] for key in temp_import.required}
                temp_import.fields.update({"defaults": defaults})
                #temp_import.fields = {field: value, 'defaults': defaults}
                return temp_import.format_data()[0]

            except IndexError:
                return self.get_or_create(object, **{field: value, 'defaults': defaults})[0]
        else:
            return None

    def format_date(self, str, format="%d.%m.%Y"):
        try:
            return timezone.make_aware(datetime.datetime.strptime(str.strip(), format), timezone.get_current_timezone())
        except ValueError:
            # return timezone.make_aware(datetime.datetime.strptime("01.01.1990", "%d.%m.%Y"), timezone.get_current_timezone())
            return timezone.now()
