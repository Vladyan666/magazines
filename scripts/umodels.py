# -*- coding: utf8 -*-
from __future__ import unicode_literals, print_function

import os
import sys
import re
import hashlib
import unidecode


from inspect import isclass

import django
from django.apps import apps

from updater import ImportData


class ImportCategory(ImportData):
    def __init__(self, language):
        super(ImportCategory, self).__init__(language=language)   
        
        self.file_name = 'category'
        self.object = apps.get_model('shop', 'Category')
        self.title = 'category'
        self.required = ('name',)
        self.fk_fields = (('host', apps.get_model('sites', 'Site'), 'domain'),)    

class ImportItem(ImportData):
    def __init__(self, language):
        super(ImportItem, self).__init__(language=language)   
        
        self.file_name = 'item'
        self.object = apps.get_model('shop', 'Item')
        self.title = 'item'
        self.required = ('name',)
        self.fk_fields = (('category',apps.get_model('shop', 'Category'),'name'), ('host', apps.get_model('sites', 'Site'), 'domain'))
        
""" 
### MailBase ###
        
#Текстовые страницы
class ImportTextMPages(ImportData):
    def __init__(self, language):
        super(ImportTextMPages, self).__init__(language=language)   
        
        self.file_name = 'mpages'
        self.object = apps.get_model('mailbase', 'Textpage')
        self.title = 'mpages'
        self.required = ('name',)
        
#Пары почтапароль
class ImportMailbase(ImportData):
    def __init__(self, language):
        super(ImportMailbase, self).__init__(language=language)   
        
        self.file_name = 'mailpass'
        self.object = apps.get_model('mailbase', 'Mailpass')
        self.title = 'mailpass'
        self.required = ('name',)
        self.fk_fields = (('mail',apps.get_model('mailbase', 'Mail'),'name'), ('base',apps.get_model('mailbase', 'Base'),'name'))
        
    def callback_func(self, qwe):
        from mailbase.models import Mail, Domain, Mailpass
        domain = Domain.objects.get_or_create(name=qwe[0].mail.name.split('@')[1])
        try:
            Mail.objects.update_or_create(name=qwe[0].mail,defaults={
                'domain' : domain[0]
            })
        
        except Mail.DoesNotExist:
            pass

           
        finally:
            pass
                
                
class ImportEventsPages(ImportData):
    def __init__(self, language):
        super(ImportEventsPages, self).__init__(language=language)   
        
        self.file_name = 'eventpages'
        self.object = apps.get_model('top50', 'Textpage')
        self.title = 'eventpages'
        self.required = ('maintitle',)
       
class ImportComment(ImportData):
    def __init__(self, language):
        super(ImportComment, self).__init__(language=language)   
        
        self.file_name = 'comment'
        self.object = apps.get_model('notificator', 'Comment')
        self.title = 'comment'
        self.required = ('name',)

class ImportGroupmessage(ImportData):
    def __init__(self, language):
        super(ImportGroupmessage, self).__init__(language=language)   
        
        self.file_name = 'groupmessage'
        self.object = apps.get_model('notificator', 'Groupmessage')
        self.title = 'groupmessage'
        self.required = ('name',)

#Проекты
class ImportQueries(ImportData):
    def __init__(self, language):
        super(ImportQueries, self).__init__(language=language)
        
        self.file_name = 'queries'
        self.object = apps.get_model('top50', 'Query')
        self.title = 'queries'
        self.required = ('text',)  

#Магазины
class ImportStore(ImportData):
    def __init__(self, language):
        super(ImportStore, self).__init__(language=language)
        
        self.file_name = 'store'
        self.object = apps.get_model('kassa', 'Store')
        self.title = 'store'
        self.required = ('store',)  
#Товары
class ImportOrder(ImportData):
    def __init__(self, language):
        super(ImportOrder, self).__init__(language=language)   
        
        self.file_name = 'order'
        self.object = apps.get_model('kassa', 'Order')
        self.title = 'order'
        self.fk_fields = (('store',apps.get_model('kassa', 'Store'),'store'),)
        self.date_fields = (('order_date','%d.%m.%Y %H:%M:%S'),)
        self.required = ('order_id',)    

#Продавец
class ImportSaler(ImportData):
    def __init__(self, language):
        super(ImportSaler, self).__init__(language=language)   
        
        self.file_name = 'saler'
        self.object = apps.get_model('bileti', 'Saler')
        self.title = 'saler'
        self.required = ('saler_id',) 
#Матч билеты
class ImportMatch(ImportData):
    def __init__(self, language):
        super(ImportMatch, self).__init__(language=language)   
        
        self.file_name = 'match'
        self.object = apps.get_model('bileti', 'Match')
        self.title = 'match'
        self.required = ('match',)  

#Билеты
class ImportBilet(ImportData):
    def __init__(self, language):
        super(ImportBilet, self).__init__(language=language)   
        
        self.file_name = 'bileti'
        self.object = apps.get_model('bileti', 'Bilet')
        self.title = 'bileti'
        self.fk_fields = (('saler',apps.get_model('bileti', 'Saler'),'saler_id'),('match',apps.get_model('bileti', 'Match'),'match'),)
        self.required = ('bilet_id',)  

#Билеты
class ImportZakaz(ImportData):
    def __init__(self, language):
        super(ImportZakaz, self).__init__(language=language)   
        
        self.file_name = 'zakaz'
        self.object = apps.get_model('bileti', 'Zakaz')
        self.title = 'zakaz'
        self.required = ('zakaz_id',)
          
#СМС
class ImportPhone(ImportData):
    def __init__(self, language):
        super(ImportPhone, self).__init__(language=language)
        
        self.file_name = 'phone'
        self.object = apps.get_model('sms', 'Phone')
        self.title = 'phone'
        self.date_fields = (('datetime','%d.%m.%Y'),)
        self.required = ('phone',)
        
        
####OVERLIKES#####        

class ImportTarif(ImportData):
    def __init__(self, language):
        super(ImportTarif, self).__init__(language=language)
        
        self.file_name = 'tarif'
        self.object = apps.get_model('overlikes', 'Tarif')
        self.title = 'tarif'
        self.required = ('tarif',)
        
class ImportSocial(ImportData):
    def __init__(self, language):
        super(ImportSocial, self).__init__(language=language)
        
        self.file_name = 'social'
        self.object = apps.get_model('overlikes', 'Social')
        self.title = 'social'
        self.required = ('hash',)

class ImportMember(ImportData):
    def __init__(self, language):
        super(ImportMember, self).__init__(language=language)
        
        self.file_name = 'member'
        self.object = apps.get_model('overlikes', 'Member')
        self.title = 'member'
        self.required = ('hash',)
        self.fk_fields = (('tarif',apps.get_model('overlikes', 'Tarif'),'tarif'),)
        

class ImportInsta_task(ImportData):
    def __init__(self, language):
        super(ImportInsta_task, self).__init__(language=language)
        
        self.file_name = 'insta_task'
        self.object = apps.get_model('overlikes', 'Insta_task')
        self.title = 'insta_task'
        self.required = ('hash',)
        self.fk_fields = (('task_maker',apps.get_model('overlikes', 'Member'),'hash'),)
        self.date_fields = (('start_date','%d.%m.%Y'),('stop_date','%d.%m.%Y'))


class ImportCompleted(ImportData):
    def __init__(self, language):
        super(ImportCompleted, self).__init__(language=language)
        
        self.file_name = 'completed'
        self.object = apps.get_model('overlikes', 'Completed')
        self.title = 'completed'
        self.required = ('id',)
        self.fk_fields = (('task',apps.get_model('overlikes', 'Insta_task'),'hash'),)
        self.mtm_fields = (('users',apps.get_model('overlikes','Member'),'hash',ImportMember),)



class ImportSMS(ImportData):
    def __init__(self, language):
        super(ImportSMS, self).__init__(language=language)
        
        self.file_name = 'sms'
        self.object = apps.get_model('sms', 'SMS')
        self.title = 'sms'
        self.fk_fields = (('phone',apps.get_model('sms', 'Phone'),'phone'),)
        self.required = ('hash',)
        
class ImportMailBase(ImportData):
    def __init__(self, language):
        super(ImportMailBase, self).__init__(language=language)
        
        self.file_name = 'mails'
        self.object = apps.get_model('insta', 'MailBase')
        self.title = 'mails'
        self.required = ('mail',)
        self.date_fields = (('pub_date','%d-%m-%Y-%H:%M'),)
        
class ImportMail(ImportData):
    def __init__(self, language):
        super(ImportMail, self).__init__(language=language)
        
        self.file_name = 'mail'
        self.object = apps.get_model('mails', 'Mail')
        self.title = 'mail'
        self.required = ('hash',)
        self.date_fields = (('pub_date','%d-%m-%Y-%H:%M'),)
        self.fk_fields = (('host',apps.get_model('mails', 'Host'),'host'),('category',apps.get_model('mails', 'Category'),'category'),('base',apps.get_model('mails', 'Base'),'basename'))
        
        
class ImportEvents(ImportData):
    def __init__(self, language):
        super(ImportEvents, self).__init__(language=language)
        
        self.file_name = 'event'
        self.object = apps.get_model('top50', 'Event')
        self.title = 'event'
        self.required = ('name',)
        self.date_fields = (('startdate','%d.%m.%Y'),('finishdate','%d.%m.%Y'))
        self.mtm_fields = (('queries',apps.get_model('top50','Query'),'text',ImportQueries),)
        
    def get_default_fields(self, item):
        from unidecode import unidecode
        from django.template import defaultfilters 
        
        item['alias'] = defaultfilters.slugify(unidecode(item['alias']))
        
        return item
        
class ImportEventsProjects(ImportData):
    def __init__(self, language):
        super(ImportEventsProjects, self).__init__(language=language)
        
        self.file_name = 'events'
        self.object = apps.get_model('top50', 'Project')
        self.title = 'events'
        self.required = ('title',)
        self.mtm_fields = (('events',apps.get_model('top50','Event'),'name',ImportEvents),)


class ImportProjects(ImportData):
    def __init__(self, language):
        super(ImportProjects, self).__init__(language=language)
        
        self.file_name = 'projects'
        self.object = apps.get_model('farm', 'Project')
        self.title = 'projects'
        self.required = ('project',)
        
class ImportInstacc(ImportData):
    def __init__(self, language):
        super(ImportInstacc, self).__init__(language=language)
        
        self.file_name = 'instacc'
        self.object = apps.get_model('insta', 'Instacc')
        self.title = 'instacc'
        self.date_fields = (('lastpost','%d.%m.%Y'),('startdate','%d.%m.%Y'))
        self.required = ('inst_id',)
        self.mtm_fields = (('tag',apps.get_model('insta','Tag'),'tag'),)
        
class ImportYoutube(ImportData):
    def __init__(self, language):
        super(ImportYoutube, self).__init__(language=language)
        
        self.file_name = 'youtube'
        self.object = apps.get_model('youtube', 'Youtube')
        self.title = 'youtube'
        self.required = ('login',)
        self.mtm_fields = (('tag',apps.get_model('youtube','Tag'),'tag'),)
        
class ImportList(ImportData):
    def __init__(self, language):
        super(ImportList, self).__init__(language=language)
        
        self.file_name = 'lists'
        self.object = apps.get_model('insta', 'List')
        self.title = 'lists'
        self.required = ('hash',)
        self.mtm_fields = (('inst',apps.get_model('insta','Instacc'),'inst_id'),)
        
        
class ImportDomain(ImportData):
    def __init__(self, language):
        super(ImportDomain, self).__init__(language=language)
        
        self.file_name = 'domain'
        self.object = apps.get_model('domain', 'Domain')
        self.title = 'domain'
        self.date_fields = (('pub_date','%d-%m-%Y'),('delegrate','%d-%m-%Y-%H:%M'),)
        self.required = ('domain',)
        self.fk_fields = (('project',apps.get_model('domain', 'Project_domain'),'project'),('registrator',apps.get_model('domain', 'Access'),'hash'),)
        
class ImportAccounts(ImportData):
    def __init__(self, language):
        super(ImportAccounts, self).__init__(language=language)
        
        self.file_name = 'accounts'
        self.object = apps.get_model('farm', 'Accounts')
        self.title = 'accounts'
        self.required = ('login',)
        self.fk_fields = (('forum',apps.get_model('farm', 'Forums'),'forum'),)

class ImportService(ImportData):
    def __init__(self, language):
        super(ImportService, self).__init__(language=language)
        
        self.file_name = 'service'
        self.object = apps.get_model('otzovik', 'Service')
        self.title = 'service'
        self.required = ('name',)

class ImportAccount(ImportData):
    def __init__(self, language):
        super(ImportAccount, self).__init__(language=language)
        
        self.file_name = 'account'
        self.object = apps.get_model('otzovik', 'Account')
        self.title = 'account'
        self.required = ('login',)
#        self.date_fields = (('lastpost','%d.%m.%Y'),)
        self.fk_fields = (('service',apps.get_model('otzovik', 'Service'),'name'),)
        
class ImportWmproject(ImportData):
    def __init__(self, language):
        super(ImportWmproject, self).__init__(language=language)
        
        self.file_name = 'wmproject'
        self.object = apps.get_model('webmaster', 'Wmproject')
        self.title = 'wmproject'
        self.required = ('name',)

class ImportYandex(ImportData):
    def __init__(self, language):
        super(ImportYandex, self).__init__(language=language)
        
        self.file_name = 'webmaster'
        self.object = apps.get_model('webmaster', 'Wmstat')
        self.title = 'webmaster'
        self.date_fields = (('pub_date','%d.%m.%Y'),)
        self.required = ('hash',)
        self.fk_fields = (('wm_project',apps.get_model('webmaster', 'Wmproject'),'name'),)


class ImportForums(ImportData):
    def __init__(self, language):
        super(ImportForums, self).__init__(language=language)
        
        self.file_name = 'forums'
        self.object = apps.get_model('farm', 'Forums')
        self.title = 'forums'
        self.required = ('forum',)
        
        
class ImportPost(ImportData):
    def __init__(self, language):
        super(ImportPost, self).__init__(language=language)
        
        self.file_name = 'post'
        self.object = apps.get_model('insta', 'Post')
        self.title = 'post'
        self.date_fields = (('date','%d.%m.%Y, '),)
        self.required = ('hash',)
        
class ImportPlayer_stats(ImportData):
    def __init__(self, language):
        super(ImportPlayer_stats, self).__init__(language=language)
        
        self.file_name = 'stats'
        self.object = apps.get_model('fifamobile', 'Player_stats')
        self.title = 'stats'
        self.required = ('hash',)
        
class ImportPlayer_item(ImportData):
    def __init__(self, language):
        super(ImportPlayer_item, self).__init__(language=language)
        
        self.file_name = 'p_item'
        self.object = apps.get_model('fifamobile', 'Player_item')
        self.title = 'p_item'
        self.required = ('hash',)
        self.mtm_fields = (('stats',apps.get_model('fifamobile','Player_stats'),'hash'),)
        
class ImportCardbox(ImportData):
    def __init__(self, language):
        super(ImportCardbox, self).__init__(language=language)
        
        self.file_name = 'cardbox'
        self.object = apps.get_model('fifamobile', 'Cardbox')
        self.title = 'cardbox'
        self.required = ('name',)
        self.mtm_fields = (('player_items',apps.get_model('fifamobile','Player_item'),'hash'),)
        
class ImportUrls(ImportData):
    def __init__(self, language):
        super(ImportUrls, self).__init__(language=language)
        
        self.file_name = 'urls'
        self.object = apps.get_model('farm', 'Urls')
        self.title = 'urls'
        self.required = ('url',)
        self.fk_fields = (('project',apps.get_model('farm', 'Project'),'project'),)
        
class ImportPostion(ImportData):
    def __init__(self, language):
        super(ImportPostion, self).__init__(language=language)
        
        self.file_name = 'position'
        self.object = apps.get_model('top50', 'Position')
        self.title = 'position'
        self.required = ('hash',)
        self.date_fields = (('pub_date','%d-%m-%Y'),)
        self.fk_fields = (('query',apps.get_model('top50', 'QueryPage'),'query'),)
        


class ImportQPositions(ImportData):
    def __init__(self, language):
        super(ImportQPositions, self).__init__(language=language)
        
        self.file_name = 'qposition'
        self.object = apps.get_model('top50', 'QPosition')
        self.title = 'qposition'
        self.required = ('hash',)
        self.date_fields = (('pub_date','%d-%m-%Y'),)
        self.fk_fields = (('query',apps.get_model('top50','Query'),'text'),)
        
    def callback_func(self,new_object):
        from top50.models import Project, QueryPage, Position
        
        new_object = new_object[0]
        count = 1
        for url in new_object.urls.split('@'):
            project_title = re.sub(r'http[s]{0,1}://','',url).split('/')[0]
            try:
                project = Project.objects.get(title=project_title)
                querypage = QueryPage.objects.get(query=new_object.query,project=project)
                position = count
                hash = hashlib.md5(project.title.encode('utf8') + new_object.query.text.encode('utf8') + str(new_object.pub_date)).hexdigest()
                pub_date = new_object.pub_date
               
                Position.objects.update_or_create(hash=hash,defaults={
                    'query' : querypage,
                    'pub_date' : pub_date,
                    'url' : url,
                    'position' : position
                })
               
            except Project.DoesNotExist:
                pass
               
            except QueryPage.DoesNotExist:
                pass
               
            finally:
                count += 1
                pass
        
        
       
class ImportQueries(ImportData):
    def __init__(self):
        super(ImportTextPages, self).__init__()
        
        from basic.models import Query
        self.file_name = 'textdata'
        self.object = Query
        self.title = 'queries'
        self.required = ('text',)      

class ImportQueryPages(ImportData):
    def __init__(self):
        super(ImportTextPages, self).__init__()
        
        from basic.models import QueryPage, Query, Project
        self.file_name = 'textdata'
        self.object = QueryPage
        self.title = 'querypages'
        self.required = ('query__text','url',)     
        self.fk_fields = (('query',Query,'text',ImportQueries),('project',Project,'title',ImportProjects),)
        
class ImportPositions(ImportData):
    def __init__(self):
        super(ImportTextPages, self).__init__()
        
        from basic.models import Position, QueryPage, Query
        self.file_name = 'textdata'
        self.object = Position
        self.title = 'positions'
        self.required = ('hash',)     
        self.fk_fields = (('query',QueryPage,'text',ImportQueryPages),('project',Project,'title',ImportProjects),)
        

"""    

 
RULES = {
    'item' : ImportItem,
    'category' : ImportCategory,
}


def init_rules(obj_name):
    if obj_name is None:
        print("Need the model name!")
        return

    return RULES.get(obj_name, None)


if __name__ == '__main__':
    # reload(sys)
    # sys.setdefaultencoding('utf8')
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "base.settings")  # указать название своего приложение (к примеру myapp.settings)
    django.setup()

    language = 'ru'
    if len(sys.argv) >= 3:
        language = sys.argv[2] or 'ru'

    title = sys.argv[1]
    callback = init_rules(title)
    if isclass(callback) and issubclass(callback, ImportData):
        instance = callback(language=language)
        instance.run()
