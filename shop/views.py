# -*- coding: utf-8 -*-
import datetime
import re, sys, os
import random
import requests
import json
import csv
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.core.mail import send_mail, BadHeaderError
from django.contrib.sitemaps import Sitemap
from django.template import RequestContext, loader
from django.db.models import Q
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import redirect
from django.contrib.sites.models import Site

# импортируем модели
from .models import Textpage, Category, Subcategory, Item, Client, Order, PayMethod, ClientsAndOrders, Scripts, Robots, CommentAuths, CommentVk, Pouch, Userinfo, Fake_seller

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias
    
def default_context(request,alias,object):
    menu = Textpage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    
    if request.get_host() == 'get-shop.net':
        domain = 'royal-accs.net'
    elif request.get_host() == 'royal-accs.net':
        domain = 'royal-accs.net'
    elif request.get_host() == 'go-accs.com':
        domain = 'go-accs.com'
    elif request.get_host() == 'best-accs.org':
        domain = 'go-accs.com'
    
    data = get_object_or_404(object, alias=alias, host__name=domain)
    scripts = Scripts.objects.filter(host__name=domain)
    curr_host = request.get_host()
    f_seller = Fake_seller.objects.filter(host__name=domain)
    
    if request.get_host() == 'get-shop.net':
        template_prefix = 'getshop'
    elif request.get_host() == 'royal-accs.net':
        template_prefix = 'getshop'
    elif request.get_host() == 'sweet-acc.ru':
        template_prefix = 'main'
    elif request.get_host() == 'go-accs.com':
        template_prefix = 'bestaccs'
    elif request.get_host() == 'best-accs.org':
        template_prefix = 'bestaccs'

    context_object = {
        'menu' : menu,
        'data' : data,
        'scripts' : scripts,
        'template_prefix' : template_prefix,
        'curr_host': curr_host,
        'domain': domain,
        'f_seller': f_seller,
    }
    
    return context_object
    
def comments(request):
    context_data = default_context( request , 'reviews' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'comments.html')
    
    if context_data['domain'] == 'go-accs.com':
        client_id = '6405035'
        client_secret = 'fACotq6hJYfDFx369Yt0'
    elif context_data['domain'] == 'royal-accs.net':
        client_id = '6350467'
        client_secret = '1IPjkPuSxUm1xl5mXJu6'
    
    userdata = ''
    if request.GET.get('code'):
        code = request.GET['code']
        first = requests.get('https://oauth.vk.com/access_token?client_id=' + client_id + '&client_secret=' + client_secret + '&redirect_uri=http://' + request.get_host() +'/reviews&code=' + code).json()
        if first['access_token']:
            access_token = first['access_token']
            uid = str(first['user_id'])
            fields = 'uid,first_name,last_name,photo_big'
            second = requests.get('https://api.vk.com/method/users.get?user_id=' + uid + '&fields=' + fields + '&access_token=' + access_token + '&v=5.73').json()
            userdata = dict(
                name=second['response'][0]['first_name'],
                surename=second['response'][0]['last_name'],
                uid=second['response'][0]['id'],
                photo_big=second['response'][0]['photo_big']
            )
            user = Userinfo.get_session(request=request)
            user.name = userdata['name']
            user.surename = userdata['surename']
            user.vk_id = userdata['uid']
            user.photo_big = userdata['photo_big']
            user.save()
    try:
        user = Userinfo.get_session(request=request)
        
        if user.surename == None:
            k = 1 / 0
        userdata = dict(
            name = user.name,
            surename = user.surename,
            uid = user.vk_id,
            photo_big = user.photo_big,
        )
        com = CommentVk.objects.filter(host__name=context_data['domain']).exclude(~Q(vkid=user.vk_id), view=False).order_by('-id')
    except:
        com = CommentVk.objects.filter(host__name=context_data['domain']).exclude(view=False).order_by('-id')
        
    
    paginator = Paginator(com, 20)
    page = request.GET.get('page')
    
    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)
        
    count = CommentVk.objects.filter(view=True, host__name=context_data['domain']).count()
    context_data.update({
	    'userdata' : userdata,
        'count' : count,
        'comments' : comments,
    })

    return HttpResponse(template.render(context_data))
    
def soc_auth(request):

    template = loader.get_template('getshop' + '/' + 'soc_auth.html')

    if request.GET.get('code'):
        code = request.GET['code']
        first = requests.get('https://oauth.vk.com/access_token?client_id=6350467&client_secret=1IPjkPuSxUm1xl5mXJu6&redirect_uri=http://royal-accs.net/auth&code=' + code).json()
        if first['access_token']:
            access_token = first['access_token']
            uid = str(first['user_id'])
            fields = 'uid,first_name,last_name,photo_big'
            second = requests.get('https://api.vk.com/method/users.get?user_id=' + uid + '&fields=' + fields + '&access_token=' + access_token + '&v=5.73').json()
#             userdata = dict(
#                 name=second['response'][0]['first_name'],
#                 surename=second['response'][0]['last_name'],
#                 uid=second['response'][0]['uid'],
#                 photo_big=second['response'][0]['photo_big']
#             )
            userdata = ''
    else:
        userdata = ''
        second = ''
            
    context_data = {
	    'userdata' : userdata,
	    'second' : second,
    }

    return HttpResponse(template.render(context_data))

def index(request):

    context_data = default_context( request , "index" , Textpage )
    
    template = loader.get_template(context_data['template_prefix'] + '/' + 'index.html')
    
    c = {}
    c.update(csrf(request))
    
    table = Item.objects.filter(host__name=context_data['domain']).order_by('menuposition')
    if request.GET.get('price'):
        get_price = request.GET['price']
        if request.GET['price'].split('_')[0] == 'max':
            table = Item.objects.filter(host__name=context_data['domain'], price__lte=int(request.GET['price'].split('_')[1])).order_by('menuposition').order_by('price')
        elif request.GET['price'].split('_')[0] == 'under':
            table = Item.objects.filter(host__name=context_data['domain'], price__gte=int(request.GET['price'].split('_')[1].split('-')[0]), price__lte=int(request.GET['price'].split('_')[1].split('-')[1])).order_by('menuposition').order_by('price')
        elif request.GET['price'].split('_')[0] == 'min':
            table = Item.objects.filter(host__name=context_data['domain'], price__gte=int(request.GET['price'].split('_')[1])).order_by('menuposition').order_by('price')
            
    else:
        get_price = False
        
    paginator = Paginator(table, 15)
    page = request.GET.get('page')
    
    slider = []
    
    i = 0
    while i < 5:
        slider.append([Item.objects.exclude(Q(slider_big='') | Q(slider_big__exact=None)).filter(host__name=context_data['domain']).order_by('slider_position')[i:i+1],Item.objects.exclude(Q(slider_small='') | Q(slider_small__exact=None)).order_by('slider_position').filter(host__name=context_data['domain'])[i*2:(i+1)*2]])
        i+=1
    
    try:
        accss = paginator.page(page)
    except PageNotAnInteger:
        accss = paginator.page(1)
    except EmptyPage:
        accss = paginator.page(paginator.num_pages)

    context_data.update({
        'slider': slider,
        'accss' : accss,
        'c': c,
        'get_price': get_price,
    })

    return HttpResponse(template.render(context_data))

def agreement(request):
    context_data = default_context( request , 'agreement' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def activate(request):
    context_data = default_context( request , 'aktivaciya-klyucha-v-steam' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def activate_o(request):
    context_data = default_context( request , 'aktivaciya-klyucha-v-origin' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def robots(request):
    if request.get_host() == 'royal-accs.net':
        domain = 'royal-accs.net'
    elif request.get_host() == 'go-accs.com':
        domain = 'go-accs.com'
    elif request.get_host() == 'best-accs.org':
        domain = 'go-accs.com'
        
    template = loader.get_template('robots.txt')
    context = Robots.objects.filter(host__name=domain)
    data = {'context':context}

    return HttpResponse(template.render(data), content_type="text/plain")
    
def get_fields(request):

    if request.get_host() == 'get-shop.net':
        domain = 'royal-accs.net'
    elif request.get_host() == 'royal-accs.net':
        domain = 'royal-accs.net'
    elif request.get_host() == 'go-accs.com':
        domain = 'go-accs.com'
    elif request.get_host() == 'best-accs.org':
        domain = 'go-accs.com'

    if request.is_ajax():
        q = request.GET.get('term', '')
        drugs = Item.objects.filter(name__icontains = q, host__name = domain)[:8]
        results = []
        for drug in drugs:
            drug_json = {}
            drug_json['img'] = drug.pic.url
            drug_json['label'] = drug.name
            drug_json['value'] = drug.alias
            drug_json['price'] = drug.price
            drug_json['category'] = drug.category.alias
            results.append(drug_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
    
def textpage(request, alias):
    context_data = default_context( request , alias , Textpage )
    if alias == Textpage.objects.get(maintitle="Категории", host__name=context_data['domain']).alias:
        template = loader.get_template(context_data['template_prefix'] + '/' + 'categories.html')
        categories = Category.objects.filter(host__name=context_data['domain'])

        context_data.update({ 
            'categories': categories,
        })
    else:
        template = loader.get_template(context_data['template_prefix'] + '/' + 'textpage.html')
        try:
            fail_mail = request.session['fail_mail']
        except:
            fail_mail = ''
            
    context_data.update({
        'fail_mail' : fail_mail,
    })

    return HttpResponse(template.render(context_data))

def clientspage(request):
    context_data = default_context(request, "clients", Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'clients.html')
    items = ClientsAndOrders.objects.all()

    context_data.update({
        'clients' : items,
    })

    return HttpResponse(template.render(context_data))

def product(request, alias, category_alias, category_page):
    context_data = default_context(request, alias, Item)
    get_object_or_404(Category, alias=category_alias, host__name=context_data['domain'])
    get_object_or_404(Textpage, alias=category_page, host__name=context_data['domain'])
    template = loader.get_template(context_data['template_prefix'] + '/' + 'product.html')
    usertoken = request.session.get(settings.CART_SESSION_ID)
        
    rand_num = str(random.randint(1000000, 9999999))
    pouches = Pouch.objects.filter(host__name=context_data['domain'])
    
    context_data.update({
        'usertoken' : usertoken,
        'pouches' : pouches,
        'rand_num' : rand_num,
    })

    return HttpResponse(template.render(context_data))
    
@csrf_exempt
def paypage(request):
    context_data = default_context(request, 'paypage', Textpage)
    
    template = loader.get_template(context_data['template_prefix'] + '/' + 'paypage.html')

    if request.method == 'POST':
        if request.POST['method'] == 'qiwi':
            method = request.POST['method']
            sum = request.POST['sum']
            tovar = request.POST['tovar']
            qiwi_phone = request.POST['qiwi_phone']
            context_data.update({
                'qiwi_phone': qiwi_phone,
                'method': method,
                'sum': sum,
                'tovar': tovar,
            })
        else:
            method = request.POST['method']
            sum = request.POST['sum']
            tovar = request.POST['tovar']
            ya_type = request.POST['ya_type']
            successs = request.POST['successURL']
            koshel = request.POST['koshel']
            context_data.update({
                'ya_type': ya_type,
                'successs': successs,
                'method': method,
                'sum': sum,
                'tovar': tovar,
                'koshel': koshel,
            })

    return HttpResponse(template.render(context_data))
    
def test(request):
    context_data = default_context(request, 'test', Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'test.html')
    
    session = vk.Session(access_token='4fb22c0b4fb22c0b4fb22c0bb84fd2ca8844fb24fb22c0b15dc2e8b7ca03d826e83497d')
    vk_api = vk.API(session)
    test = vk_api.users.get(user_id=1, fields="first_name,last_name,photo_big, uid")
    
    context_data.update({
        'test' : test,
    })

    return HttpResponse(template.render(context_data))

def categories(request):
    context_data = default_context(request, "categories", Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'categories.html')
    categories = Category.objects.filter(host__name=context_data['domain'])
    
    get_object_or_404(Textpage, alias=category_page, host__name=domain)

    context_data.update({ 'categories': categories, })

    return HttpResponse(template.render(context_data))
    
def check_purchases(request):
    if request.is_ajax():
        if request.POST['type'] == 'what_i_buy':
            items = []
            for i in Item.objects.filter(client__mail=request.POST['mail'], host__name=request.get_host()):
                items.append({'order_id' : i.order_id, 'item' : i.item.name, 'date' : i.buy_date.strftime('%d-%m-%Y'), 'paytype' : i.paymethod.paymethod, 'price' : i.item.price })
            return HttpResponse(json.dumps(items, ensure_ascii=False).encode('utf8'))
    else:
        context_data = default_context(request, 'check-purchases', Textpage)
        template = loader.get_template(context_data['template_prefix'] + '/' + 'purchases.html')    
        

        context_data.update({
            'categories': categories,
        })

        return HttpResponse(template.render(context_data))

def ord_succes(request):
    context_data = default_context(request, 'order-success', Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'order-success.html')
    user = Userinfo.get_session(request=request)
    
    if user.mail != None:
        fail_mail = user.mail
    else:
        fail_mail = ''
    
    context_data.update({
        'fail_mail' : fail_mail,
    })

    return HttpResponse(template.render(context_data))
    
def category(request, alias, category_page):
    context_data = default_context(request, alias, Category)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'category.html')
    
    get_object_or_404(Textpage, alias=category_page, host__name=context_data['domain'])
    
    table = Item.objects.filter(category__alias=alias, host__name=context_data['domain']).order_by('menuposition')
            
    if request.GET.get('price'):
        get_price = request.GET['price']
        if request.GET['price'].split('_')[0] == 'max':
            table = Item.objects.filter(host__name=context_data['domain'], price__lte=int(request.GET['price'].split('_')[1])).order_by('menuposition')
        elif request.GET['price'].split('_')[0] == 'under':
            table = Item.objects.filter(host__name=context_data['domain'], price__gte=int(request.GET['price'].split('_')[1].split('-')[0]), price__lte=int(request.GET['price'].split('_')[1].split('-')[1])).order_by('menuposition')
        elif request.GET['price'].split('_')[0] == 'min':
            table = Item.objects.filter(host__name=context_data['domain'], price__gte=int(request.GET['price'].split('_')[1])).order_by('menuposition')

    context_data.update({ 
        'items': table,
    })

    return HttpResponse(template.render(context_data))   
    
def check_order(token):
    api_access_token = 'e3a2a373dfe2bfdebf5aa83550d7198f' 
    my_login = '+79252757855'

    s = requests.Session()
    s.headers['authorization'] = 'Bearer ' + api_access_token
    parameters = {'rows': '1'}
    h = s.get('https://edge.qiwi.com/payment-history/v1/persons/'+my_login+'/payments', params = parameters)
    a = json.loads(h.text)
    for i in a['data']:
        if token == i['comment']:
            order_item = Order.objects.get(hash=token)
            if order_item.status != u'Выдан':
                if (i['sum'])['amount'] >= order_item.price:
                    counter = order_item.count
                    items = order_item.item.items.split('\r\n')
                    otdat_pokupatelyu = ''
                    vernut_nazad = ''
                    for items_item in items:
                        if counter > 0:
                            otdat_pokupatelyu += items_item+'\r\n'
                            counter = counter - 1
                        else:
                            vernut_nazad += items_item+'\r\n'
                    Item.objects.filter(id=order_item.item.id).update(items=vernut_nazad, count=(order_item.item.count-order_item.count))
                    order_item.status = u'Выдан'
                    order_item.items = otdat_pokupatelyu
                    order_item.save()
                    return otdat_pokupatelyu
            else:
                return order_item.items
        else:
            return 'Неоплачено'
            
def ajax(request):
    if request.is_ajax():
        # create_task
        if request.POST['type'] == 'create_order':
            client, created = Client.objects.get_or_create(mail=request.POST['mail'])
            order, created = Order.objects.get_or_create(order_id=666,paymethod=PayMethod.objects.get(id=request.POST['paymethod']),
                                client=client, item=Item.objects.get(id=request.POST['item_id']), count=request.POST['count'],
                                price=request.POST['total_price'], status='Заказ не оплачен', hash='zakaz' + str(random.randint(1000000, 9999999)))
            MyValue = order.hash
        elif request.POST['type'] == 'check_order':
            a = check_order(request.POST['token'])
            MyValue = a
            
            
        #Создаем корявую почту и заказ
        elif request.POST['type'] == 'save_mail':
            if request.POST['usertoken'] != "":
                item = request.POST['email'].split('@')[0]
                lengt = len(item)
                r_num = random.randint(1, lengt-2)
                ttt = item[0:r_num] + item[r_num+1:lengt] + '@' + request.POST['email'].split('@')[1]
                user, create = Userinfo.objects.get_or_create(token=request.POST['usertoken'])
                user.mail = ttt
                user.save()
                obj, create = Client.objects.get_or_create(mail=ttt)
                order = Order(order_id=int(request.POST['number']), client=obj, item=Item.objects.get(id=request.POST['id']), paymethod=PayMethod.objects.get(id=request.POST['paytype']), count=1, status=u'123', price=Item.objects.get(id=request.POST['id']).price, host=Site.objects.get(name=request.get_host()))
                order.save()
                MyValue = '123'
            else:
                MyValue = '123'
                
        elif request.POST['type'] == 'add_comment':
            if request.get_host() == 'go-accs.com':
                host = 'best-accs.org'
            elif request.get_host() == 'royal-accs.net':
                host = 'royal-accs.net'
            else:
                host = 'royal-accs.net'
                
            if CommentVk.objects.filter(vkid=request.POST['data[vk_id]']).count() > 3:
                MyValue = '234'
            else:
                item = CommentVk(vkid=request.POST['data[vk_id]'], comment=request.POST['data[text]'], avatar=request.POST['data[img]'], username=request.POST['data[username]'], time=request.POST['data[time]'], host=Site.objects.get(name=request.get_host()))
                item.save()
                MyValue = '123'
    return HttpResponse(MyValue)