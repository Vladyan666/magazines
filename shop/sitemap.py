﻿import datetime
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from django.contrib.sitemaps.views import x_robots_tag
from django.utils.functional import cached_property
from django.contrib.sites.models import Site

from .models import Textpage, Category, Subcategory, Item

class BaseSitemap(Sitemap):
    @cached_property
    def current_site(self):
        return Site.objects.get_current()

class TextpageSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Textpage.objects.filter(host=self.current_site).all()
        
class CategorySitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Category.objects.filter(host=self.current_site).all()
        
class SubcategorySitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Subcategory.objects.filter(host=self.current_site).all()
        
class ItemSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Item.objects.filter(host=self.current_site).all()
        