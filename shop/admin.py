# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.sites.models import Site

from shop.models import Textpage, Category, Subcategory, Item, PayMethod, Client, Order, ClientsAndOrders, Scripts, Robots, CommentVk, Pouch, Fake_seller, Userinfo

# list_filter с исключенными страницами других доменов
class DomainFilter(admin.RelatedFieldListFilter):
    def field_choices(self, field, request, model_admin): 
        choices = field.get_choices(include_blank=False)
        choices = [choice for choice in choices if field.related_model.objects.filter(pk=choice[0],host__name=request.get_host()).exists()]
        choices.sort(key=lambda i: i[1])
        return choices

# Register your models here.
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow', 'host','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','canonical','content',]})
]
page_list = ('host', 'menushow','sitemap')

#Страницы
class TextpageAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['maintitle', 'menuposition',]}),] + page_fields
    list_filter = ['host']
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("maintitle",)}
    
admin.site.register(Textpage, TextpageAdmin)

#Категории
class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','background','genre','youtube','release','language','razrab','osob','creator', 'comment', 'domain']}),] + page_fields
    list_filter = ['host']
    
    def get_prepopulated_fields(self, request, obj=None):
        return {"alias": ("name",)}
    
admin.site.register(Category, CategoryAdmin)

class CommentVkAdmin(admin.ModelAdmin):
    list_display = ('username','vkid','comment', 'news_comment', 'view', 'host')
    fieldsets = [(u'Основные', {'fields': ['vkid', 'comment', 'username', 'news_comment', 'view', 'time','avatar', 'host']}), ]
    list_editable = ['news_comment', 'view']
    list_filter = ['host']
#
admin.site.register(CommentVk, CommentVkAdmin)

#Userinfo
class UserinfoAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','surename', 'vk_id', 'photo_big', 'mail']}),]
    
admin.site.register(Userinfo, UserinfoAdmin)

#Подкатегории
class SubcategoryAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'category', 'comment',]}),] + page_fields
    list_filter = ['host']
    
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "category":
#             kwargs["queryset"] = Category.objects.filter(host__name=request.get_host())
#         return super(SubcategoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
admin.site.register(Subcategory, SubcategoryAdmin)

class PouchAdmin(admin.ModelAdmin):
    list_display = ('number', 'type', 'host')
    fieldsets = [(u'Основные',  {'fields':['number', 'type', 'host']}),]
    list_filter = ['host']

admin.site.register(Pouch, PouchAdmin)

class Fake_sellerAdmin(admin.ModelAdmin):
    list_display = ('name', 'suername', 'second_name', 'host')
    fieldsets = [(u'Основные',  {'fields':['name', 'suername', 'second_name', 'nickname', 'webmoney', 'img', 'birth', 'deposit', 'success_trade', 'fail_trade', 'host']}),]
    list_filter = ['host']

admin.site.register(Fake_seller, Fake_sellerAdmin)

#Товары
class ItemAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'price','menuposition','category', 'fake_seller','categories', 'min_pay', 'autor', 'count', 'items', 'description', 'pic', 'itemimage', 'video','background','discount', 'comment', 'slider_big', 'slider_small', 'slider_position']}),] + page_fields
    list_filter = ['host', ('category', DomainFilter)]
    list_display = ('id','name','alias','menuposition', 'fake_seller', 'category', 'description','price','discount')
    list_editable = ['alias','price','menuposition','description', 'fake_seller','category','discount']
    search_fields = ('name','alias')
    
    def get_queryset(self, request):
        queryset = super(ItemAdmin, self).get_queryset(request)
        
        return queryset.filter(host__name=request.get_host())
    
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}
    
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "category":
#             kwargs["queryset"] = Category.objects.filter(host__name='royal')
#         return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # if db_field.name == "host":
            # kwargs["queryset"] = Site.objects.filter(name=request.get_host())
        # return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    
admin.site.register(Item, ItemAdmin)

class PayMethodAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['paymethod', 'token', 'phone']}),]
    
admin.site.register(PayMethod, PayMethodAdmin)

class ClientAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['mail']}),]
    
admin.site.register(Client, ClientAdmin)

class OrderAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['order_id', 'hash', 'paymethod', 'client', 'purse', 'item', 'count', 'items', 'status', 'price', 'host']}),]
    
admin.site.register(Order, OrderAdmin)

class ClientsAndOrdersAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['client', 'orders']}),]
    
admin.site.register(ClientsAndOrders, ClientsAndOrdersAdmin)

class ScriptsAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные', {'fields': ['name', 'content','host' ]}), ]
#
admin.site.register(Scripts, ScriptsAdmin)

class RobotsAdmin(admin.ModelAdmin):
    fieldsets = [('Содержимое', {'fields': ['content','host']}), ]

admin.site.register(Robots, RobotsAdmin)