# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-07 11:53
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hash', models.CharField(max_length=200, verbose_name='\u0425\u044d\u0448')),
                ('alias', models.SlugField(max_length=200, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menuposition', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('og_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Title')),
                ('og_description', models.TextField(blank=True, max_length=2000, null=True, verbose_name='OG Description')),
                ('og_type', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Type')),
                ('og_image', models.CharField(blank=True, max_length=500, null=True, verbose_name='OG image')),
                ('og_type_pb_time', models.DateField(default=datetime.date.today, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('og_type_author', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG author')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438')),
                ('comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hash', models.CharField(max_length=200, verbose_name='\u0425\u044d\u0448')),
                ('alias', models.SlugField(max_length=200, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menuposition', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('og_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Title')),
                ('og_description', models.TextField(blank=True, max_length=2000, null=True, verbose_name='OG Description')),
                ('og_type', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Type')),
                ('og_image', models.CharField(blank=True, max_length=500, null=True, verbose_name='OG image')),
                ('og_type_pb_time', models.DateField(default=datetime.date.today, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('og_type_author', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG author')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('price', models.FloatField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u0435\u0434\u0438\u043d\u0438\u0446\u0443 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('item', models.CharField(max_length=200, verbose_name='\u0421\u0442\u0440\u043e\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('min_pay', models.IntegerField(default=1, verbose_name='\u041c\u0438\u043d\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u043e\u043a\u0443\u043f\u043a\u0438')),
                ('count', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('items', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0440\u043e\u043a\u0438 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('description', models.CharField(blank=True, max_length=1000, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430(\u0434\u043e 1000 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432)')),
                ('pic', models.ImageField(blank=True, null=True, upload_to=b'', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('comment', models.CharField(blank=True, max_length=2000, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.Category', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b',
            },
        ),
    ]
