# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-19 16:23
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0040_order_host'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='buy_date',
            field=models.DateField(default=datetime.datetime(2018, 3, 19, 16, 23, 30, 700006), verbose_name='\u0412\u0440\u0435\u043c\u044f \u041f\u043e\u043a\u0443\u043f\u043a\u0438'),
        ),
    ]
