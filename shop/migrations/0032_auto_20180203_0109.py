# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-03 01:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        ('shop', '0031_auto_20180203_0013'),
    ]

    operations = [
        migrations.AddField(
            model_name='scripts',
            name='host',
            field=models.ForeignKey(default=3, on_delete=django.db.models.deletion.SET_DEFAULT, to='sites.Site', verbose_name='\u0425\u043e\u0441\u0442'),
        ),
        migrations.AlterField(
            model_name='category',
            name='canonical',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Canonical url(\u0431\u0435\u0437 \u0441\u043b\u0435\u0448\u0435\u0439 \u0432 \u043d\u0430\u0447\u0430\u043b\u0435 \u0438 \u043a\u043e\u043d\u0446\u0435)'),
        ),
        migrations.AlterField(
            model_name='item',
            name='canonical',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Canonical url(\u0431\u0435\u0437 \u0441\u043b\u0435\u0448\u0435\u0439 \u0432 \u043d\u0430\u0447\u0430\u043b\u0435 \u0438 \u043a\u043e\u043d\u0446\u0435)'),
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='canonical',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Canonical url(\u0431\u0435\u0437 \u0441\u043b\u0435\u0448\u0435\u0439 \u0432 \u043d\u0430\u0447\u0430\u043b\u0435 \u0438 \u043a\u043e\u043d\u0446\u0435)'),
        ),
        migrations.AlterField(
            model_name='textpage',
            name='canonical',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Canonical url(\u0431\u0435\u0437 \u0441\u043b\u0435\u0448\u0435\u0439 \u0432 \u043d\u0430\u0447\u0430\u043b\u0435 \u0438 \u043a\u043e\u043d\u0446\u0435)'),
        ),
    ]
