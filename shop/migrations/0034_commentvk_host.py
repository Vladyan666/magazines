# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-28 20:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        ('shop', '0033_pouch'),
    ]

    operations = [
        migrations.AddField(
            model_name='commentvk',
            name='host',
            field=models.ForeignKey(default=3, on_delete=django.db.models.deletion.SET_DEFAULT, to='sites.Site', verbose_name='\u0425\u043e\u0441\u0442'),
        ),
    ]
